import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import java.awt.GridLayout;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Vector;
import java.awt.GridBagLayout;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import java.awt.GridBagConstraints;
import javax.swing.JLayeredPane;
import java.awt.Insets;
import java.awt.List;
import java.awt.CardLayout;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JPasswordField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTable;
import javax.swing.JTabbedPane;
import javax.swing.JList;
import javax.swing.JRadioButton;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class futebolTp<E> extends JFrame {
	static final String JDBC_DRIVER = "org.sqlite.JDBC";  
	static final String DB_URL = "jdbc:sqlite:db_tp.db";
	Connection conn = null;
	Statement stmt = null;
	private JList<String> jogadorList;
	private JList<String> equipaList;
	private JPanel contentPane;
	private JTextField txtlogin;
	private JPasswordField txtpass;
	private JTable equipaTable;
	private JTable jogTable;
	private JTable melhorJogTable;
	private JTextField nomeJogTF;
	private JTextField dataJogTF;
	private JTextField nomeEquiTF;
	private JTextField nomeEstadioTF;
	private JTextField dataFunTF;
	private JTextField desigEpoTF;
	private JTextField dataIniTF;
	private JTextField dataFimTF;
	private JTable classifTable;
	private JTextField dataJogoTF;
	private JTextField goloCasaTF;
	private JTextField goloForaTF;
	private String jogadorid;
	private JTextField minutoTF;
	private String getTeams;
	private String getjogador;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					futebolTp frame = new futebolTp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	// try and catch espalhado pelo codigo sao as combobox que iram ser preenchidas com epocas
	public futebolTp() {
		try{
			//STEP 2: Register JDBC driver
			Class.forName(JDBC_DRIVER); 

			//STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL);

			//STEP 4: Execute a query
			System.out.println("Quering database...");
			stmt = conn.createStatement();

			System.out.println("Database queried successfully...");
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}// nothing we can do
		}
		//end try}

		//loadEpocaCB();
		//loginPanel.setVisible(false);
		//mainPanel.setVisible(true);
	
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 719, 537);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new CardLayout(0, 0));

		JPanel loginPanel = new JPanel();
		contentPane.add(loginPanel, "name_2411229891585926");
		GridBagLayout gbl_loginPanel = new GridBagLayout();
		gbl_loginPanel.columnWidths = new int[]{0, 0, 0};
		gbl_loginPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
		gbl_loginPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_loginPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		loginPanel.setLayout(gbl_loginPanel);

		JLabel lblNewLabel = new JLabel("Utilizador");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		loginPanel.add(lblNewLabel, gbc_lblNewLabel);

		txtlogin = new JTextField();
		GridBagConstraints gbc_txtlogin = new GridBagConstraints();
		gbc_txtlogin.insets = new Insets(0, 0, 5, 0);
		gbc_txtlogin.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtlogin.gridx = 1;
		gbc_txtlogin.gridy = 0;
		loginPanel.add(txtlogin, gbc_txtlogin);
		txtlogin.setColumns(10);

		JLabel lblNewLabel_2 = new JLabel("Password");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 1;
		loginPanel.add(lblNewLabel_2, gbc_lblNewLabel_2);

		txtpass = new JPasswordField();
		GridBagConstraints gbc_txtpass = new GridBagConstraints();
		gbc_txtpass.insets = new Insets(0, 0, 5, 0);
		gbc_txtpass.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtpass.gridx = 1;
		gbc_txtpass.gridy = 1;
		loginPanel.add(txtpass, gbc_txtpass);

		JRadioButton rdbtnAnonimo = new JRadioButton("Anonimo");
		GridBagConstraints gbc_rdbtnAnonimo = new GridBagConstraints();
		gbc_rdbtnAnonimo.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnAnonimo.gridx = 0;
		gbc_rdbtnAnonimo.gridy = 2;
		loginPanel.add(rdbtnAnonimo, gbc_rdbtnAnonimo);

		JLabel lblLoginError = new JLabel("");
		GridBagConstraints gbc_lblLoginError = new GridBagConstraints();
		gbc_lblLoginError.gridx = 1;
		gbc_lblLoginError.gridy = 4;
		loginPanel.add(lblLoginError, gbc_lblLoginError);

		JPanel mainPanel = new JPanel();
		contentPane.add(mainPanel, "name_2411229915817802");
		GridBagLayout gbl_mainPanel = new GridBagLayout();
		gbl_mainPanel.columnWidths = new int[]{0, 0, 0, 0};
		gbl_mainPanel.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_mainPanel.columnWeights = new double[]{1.0, 1.0, 1.0, Double.MIN_VALUE};
		gbl_mainPanel.rowWeights = new double[]{0.0, 0.0, 1.0, 1.0, Double.MIN_VALUE};
		mainPanel.setLayout(gbl_mainPanel);

		JPanel addPanel = new JPanel();
		contentPane.add(addPanel, "name_485813431355640");
		addPanel.setLayout(new GridLayout(1, 0, 0, 0));

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		addPanel.add(tabbedPane);

		JPanel addJogPanel = new JPanel();
		tabbedPane.addTab("jogador", null, addJogPanel, null);
		GridBagLayout gbl_addJogPanel = new GridBagLayout();
		gbl_addJogPanel.columnWidths = new int[]{0, 0, 0};
		gbl_addJogPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_addJogPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_addJogPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		addJogPanel.setLayout(gbl_addJogPanel);

		JLabel lblNewLabel_1 = new JLabel("Nome");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 1;
		addJogPanel.add(lblNewLabel_1, gbc_lblNewLabel_1);

		nomeJogTF = new JTextField();
		GridBagConstraints gbc_nomeJogTF = new GridBagConstraints();
		gbc_nomeJogTF.insets = new Insets(0, 0, 5, 0);
		gbc_nomeJogTF.fill = GridBagConstraints.HORIZONTAL;
		gbc_nomeJogTF.gridx = 1;
		gbc_nomeJogTF.gridy = 1;
		addJogPanel.add(nomeJogTF, gbc_nomeJogTF);
		nomeJogTF.setColumns(10);

		JLabel lblNewLabel_3 = new JLabel("data de Nascimento");
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_3.gridx = 0;
		gbc_lblNewLabel_3.gridy = 2;
		addJogPanel.add(lblNewLabel_3, gbc_lblNewLabel_3);

		dataJogTF = new JTextField();
		GridBagConstraints gbc_dataJogTF = new GridBagConstraints();
		gbc_dataJogTF.insets = new Insets(0, 0, 5, 0);
		gbc_dataJogTF.fill = GridBagConstraints.HORIZONTAL;
		gbc_dataJogTF.gridx = 1;
		gbc_dataJogTF.gridy = 2;
		addJogPanel.add(dataJogTF, gbc_dataJogTF);
		dataJogTF.setColumns(10);

		JLabel lblNewLabel_4 = new JLabel("Posicao");
		GridBagConstraints gbc_lblNewLabel_4 = new GridBagConstraints();
		gbc_lblNewLabel_4.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_4.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_4.gridx = 0;
		gbc_lblNewLabel_4.gridy = 3;
		addJogPanel.add(lblNewLabel_4, gbc_lblNewLabel_4);

		JComboBox<String> posicaoJogCB = new JComboBox();
		GridBagConstraints gbc_posicaoJogCB = new GridBagConstraints();
		gbc_posicaoJogCB.insets = new Insets(0, 0, 5, 0);
		gbc_posicaoJogCB.fill = GridBagConstraints.HORIZONTAL;
		gbc_posicaoJogCB.gridx = 1;
		gbc_posicaoJogCB.gridy = 3;
		addJogPanel.add(posicaoJogCB, gbc_posicaoJogCB);
		
		posicaoJogCB.addItem("Guarda-Redes");
		posicaoJogCB.addItem("Defesa");
		posicaoJogCB.addItem("Medio");
		posicaoJogCB.addItem("Atacante");
		posicaoJogCB.addItem("Lateral");

		JButton adicionarJogBtn = new JButton("Adicionar");
		//butao adicionar jogadores
		adicionarJogBtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String nome = nomeJogTF.getText();
				String dataN = dataJogTF.getText();
				String posicao = (String) posicaoJogCB.getSelectedItem();
				String sql = "INSERT INTO Jogador(nomeJogador, dataNascimento, posicaoHabitual) VALUES(?,?,?)";
				
					try( PreparedStatement stmt = conn.prepareStatement(sql)) {
			            stmt.setString(1, nome);
			            stmt.setString(2, dataN);
			            stmt.setString(3, posicao);
			            stmt.executeUpdate();
			            System.out.println("insert sucessfull");
			        } catch (SQLException e1) {
			            System.out.println(e1.getMessage());
			        }
			    
					nomeJogTF.setText("");
					dataJogTF.setText("");
					posicaoJogCB.setSelectedIndex(0);
					
			}
		});
		GridBagConstraints gbc_adicionarJogBtn = new GridBagConstraints();
		gbc_adicionarJogBtn.insets = new Insets(0, 0, 5, 0);
		gbc_adicionarJogBtn.gridx = 1;
		gbc_adicionarJogBtn.gridy = 5;
		addJogPanel.add(adicionarJogBtn, gbc_adicionarJogBtn);

		JButton btnMenu6 = new JButton("voltar ao menu");
		btnMenu6.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				addPanel.setVisible(false);
				mainPanel.setVisible(true);
			}
		});
		GridBagConstraints gbc_btnMenu6 = new GridBagConstraints();
		gbc_btnMenu6.gridx = 1;
		gbc_btnMenu6.gridy = 7;
		addJogPanel.add(btnMenu6, gbc_btnMenu6);

		JPanel addEquiPanel = new JPanel();
		tabbedPane.addTab("equipa", null, addEquiPanel, null);
		GridBagLayout gbl_addEquiPanel = new GridBagLayout();
		gbl_addEquiPanel.columnWidths = new int[]{0, 0, 0};
		gbl_addEquiPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_addEquiPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_addEquiPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		addEquiPanel.setLayout(gbl_addEquiPanel);

		JLabel lblNewLabel_5 = new JLabel("Nome da equipa");
		GridBagConstraints gbc_lblNewLabel_5 = new GridBagConstraints();
		gbc_lblNewLabel_5.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_5.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_5.gridx = 0;
		gbc_lblNewLabel_5.gridy = 0;
		addEquiPanel.add(lblNewLabel_5, gbc_lblNewLabel_5);

		nomeEquiTF = new JTextField();
		GridBagConstraints gbc_nomeEquiTF = new GridBagConstraints();
		gbc_nomeEquiTF.insets = new Insets(0, 0, 5, 0);
		gbc_nomeEquiTF.fill = GridBagConstraints.HORIZONTAL;
		gbc_nomeEquiTF.gridx = 1;
		gbc_nomeEquiTF.gridy = 0;
		addEquiPanel.add(nomeEquiTF, gbc_nomeEquiTF);
		nomeEquiTF.setColumns(10);

		JLabel lblNewLabel_6 = new JLabel("Nome do estadio");
		GridBagConstraints gbc_lblNewLabel_6 = new GridBagConstraints();
		gbc_lblNewLabel_6.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_6.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_6.gridx = 0;
		gbc_lblNewLabel_6.gridy = 1;
		addEquiPanel.add(lblNewLabel_6, gbc_lblNewLabel_6);

		nomeEstadioTF = new JTextField();
		GridBagConstraints gbc_nomeEstadioTF = new GridBagConstraints();
		gbc_nomeEstadioTF.insets = new Insets(0, 0, 5, 0);
		gbc_nomeEstadioTF.fill = GridBagConstraints.HORIZONTAL;
		gbc_nomeEstadioTF.gridx = 1;
		gbc_nomeEstadioTF.gridy = 1;
		addEquiPanel.add(nomeEstadioTF, gbc_nomeEstadioTF);
		nomeEstadioTF.setColumns(10);

		JLabel lblNewLabel_7 = new JLabel("Data de fundacao");
		GridBagConstraints gbc_lblNewLabel_7 = new GridBagConstraints();
		gbc_lblNewLabel_7.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_7.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_7.gridx = 0;
		gbc_lblNewLabel_7.gridy = 2;
		addEquiPanel.add(lblNewLabel_7, gbc_lblNewLabel_7);

		dataFunTF = new JTextField();
		GridBagConstraints gbc_dataFunTF = new GridBagConstraints();
		gbc_dataFunTF.insets = new Insets(0, 0, 5, 0);
		gbc_dataFunTF.fill = GridBagConstraints.HORIZONTAL;
		gbc_dataFunTF.gridx = 1;
		gbc_dataFunTF.gridy = 2;
		addEquiPanel.add(dataFunTF, gbc_dataFunTF);
		dataFunTF.setColumns(10);

		JButton addEquiBtn = new JButton("adicionar equipa");
		//butao dicionar equipa
		addEquiBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		addEquiBtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				String nomeE = nomeEquiTF.getText();
				String nomeETS = nomeEstadioTF.getText();
				String dataF = dataFunTF.getText();
				String sql = "INSERT INTO Equipa(nomeEquipa, nomeEstadio, dataFundacao) VALUES(?,?,?)";
						
						
						
					try( PreparedStatement stmt = conn.prepareStatement(sql)) {
			            stmt.setString(1, nomeE);
			            stmt.setString(2, nomeETS);
			            stmt.setString(3, dataF);
			            stmt.executeUpdate();
			            System.out.println("insert sucessfull");
			        } catch (SQLException e1) {
			            System.out.println(e1.getMessage());
			        }
			
			nomeEquiTF.setText("");
			nomeEstadioTF.setText("");
			dataFunTF.setText("");
			
			}
		});
		GridBagConstraints gbc_addEquiBtn = new GridBagConstraints();
		gbc_addEquiBtn.insets = new Insets(0, 0, 0, 5);
		gbc_addEquiBtn.gridx = 0;
		gbc_addEquiBtn.gridy = 5;
		addEquiPanel.add(addEquiBtn, gbc_addEquiBtn);

		JButton btnMenu5 = new JButton("voltar ao menu");
		btnMenu5.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				addPanel.setVisible(false);
				mainPanel.setVisible(true);
			}
		});
		GridBagConstraints gbc_btnMenu5 = new GridBagConstraints();
		gbc_btnMenu5.gridx = 1;
		gbc_btnMenu5.gridy = 5;
		addEquiPanel.add(btnMenu5, gbc_btnMenu5);
		
		JPanel addEpoPanel = new JPanel();
		tabbedPane.addTab("epoca", null, addEpoPanel, null);
		GridBagLayout gbl_addEpoPanel = new GridBagLayout();
		gbl_addEpoPanel.columnWidths = new int[]{0, 0, 0};
		gbl_addEpoPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
		gbl_addEpoPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_addEpoPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, 1.0, Double.MIN_VALUE};
		addEpoPanel.setLayout(gbl_addEpoPanel);
				
		JLabel lblNewLabel_8 = new JLabel("desgina\u00E7ao da epoca");
		GridBagConstraints gbc_lblNewLabel_8 = new GridBagConstraints();
		gbc_lblNewLabel_8.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_8.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_8.gridx = 0;
		gbc_lblNewLabel_8.gridy = 0;
		addEpoPanel.add(lblNewLabel_8, gbc_lblNewLabel_8);
						
		desigEpoTF = new JTextField();
		GridBagConstraints gbc_desigEpoTF = new GridBagConstraints();
		gbc_desigEpoTF.insets = new Insets(0, 0, 5, 0);
		gbc_desigEpoTF.fill = GridBagConstraints.HORIZONTAL;
		gbc_desigEpoTF.gridx = 1;
		gbc_desigEpoTF.gridy = 0;
		addEpoPanel.add(desigEpoTF, gbc_desigEpoTF);
		desigEpoTF.setColumns(10);
								
		JLabel lblNewLabel_9 = new JLabel("data de inicio");
		GridBagConstraints gbc_lblNewLabel_9 = new GridBagConstraints();
		gbc_lblNewLabel_9.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_9.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_9.gridx = 0;
		gbc_lblNewLabel_9.gridy = 1;
		addEpoPanel.add(lblNewLabel_9, gbc_lblNewLabel_9);
										
		dataIniTF = new JTextField();
		GridBagConstraints gbc_dataIniTF = new GridBagConstraints();
		gbc_dataIniTF.insets = new Insets(0, 0, 5, 0);
		gbc_dataIniTF.fill = GridBagConstraints.HORIZONTAL;
		gbc_dataIniTF.gridx = 1;
		gbc_dataIniTF.gridy = 1;
		addEpoPanel.add(dataIniTF, gbc_dataIniTF);
		dataIniTF.setColumns(10);
												
		JLabel lblNewLabel_10 = new JLabel("data de fim");
		GridBagConstraints gbc_lblNewLabel_10 = new GridBagConstraints();
		gbc_lblNewLabel_10.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_10.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_10.gridx = 0;
		gbc_lblNewLabel_10.gridy = 2;
		addEpoPanel.add(lblNewLabel_10, gbc_lblNewLabel_10);
														
		dataFimTF = new JTextField();
		GridBagConstraints gbc_dataFimTF = new GridBagConstraints();
		gbc_dataFimTF.insets = new Insets(0, 0, 5, 0);
		gbc_dataFimTF.fill = GridBagConstraints.HORIZONTAL;
		gbc_dataFimTF.gridx = 1;
		gbc_dataFimTF.gridy = 2;
		addEpoPanel.add(dataFimTF, gbc_dataFimTF);
		dataFimTF.setColumns(10);
																																		
		JButton addEpoBtn = new JButton("adicionar epoca");
		//butao adicionar equipa
		addEpoBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		addEpoBtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String desig = desigEpoTF.getText();
				String dataini = dataIniTF.getText();
				String datafim = dataFimTF.getText();
				String sql = "INSERT INTO Epoca(designacao, dataInicio, dataFim) VALUES(?,?,?)";
				
				try( PreparedStatement stmt = conn.prepareStatement(sql)) {
		            stmt.setString(1, desig);
		            stmt.setString(2, dataini);
		            stmt.setString(3, datafim);
		            stmt.executeUpdate();
		            System.out.println("insert sucessfull");
		        } catch (SQLException e1) {
		            System.out.println(e1.getMessage());
		        }
				
				desigEpoTF.setText("");
				dataIniTF.setText("");
				dataFimTF.setText("");
				
				
			}
		});
		GridBagConstraints gbc_addEpoBtn = new GridBagConstraints();
		gbc_addEpoBtn.insets = new Insets(0, 0, 0, 5);
		gbc_addEpoBtn.gridx = 0;
		gbc_addEpoBtn.gridy = 4;
		addEpoPanel.add(addEpoBtn, gbc_addEpoBtn);
									
		JButton btnMenu4 = new JButton("voltar ao menu");
		btnMenu4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				addPanel.setVisible(false);
				mainPanel.setVisible(true);
			}
		});
		GridBagConstraints gbc_btnMenu4 = new GridBagConstraints();
		gbc_btnMenu4.gridx = 1;
		gbc_btnMenu4.gridy = 4;
		addEpoPanel.add(btnMenu4, gbc_btnMenu4);
		
		JPanel addTodoPanel =new JPanel();
	
		tabbedPane.addTab("atribuir cada", null, addTodoPanel, null);
		GridBagLayout gbl_addTodoPanel = new GridBagLayout();
		gbl_addTodoPanel.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_addTodoPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_addTodoPanel.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_addTodoPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		addTodoPanel.setLayout(gbl_addTodoPanel);
		
		JLabel lblEscolhaUmEquipa = new JLabel("Escolha um equipa e varios jogadores");
		GridBagConstraints gbc_lblEscolhaUmEquipa = new GridBagConstraints();
		gbc_lblEscolhaUmEquipa.insets = new Insets(0, 0, 5, 5);
		gbc_lblEscolhaUmEquipa.gridx = 1;
		gbc_lblEscolhaUmEquipa.gridy = 2;
		addTodoPanel.add(lblEscolhaUmEquipa, gbc_lblEscolhaUmEquipa);
		
		JLabel lblNewLabel_14 = new JLabel("Epoca");
		GridBagConstraints gbc_lblNewLabel_14 = new GridBagConstraints();
		gbc_lblNewLabel_14.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_14.gridx = 4;
		gbc_lblNewLabel_14.gridy = 2;
		addTodoPanel.add(lblNewLabel_14, gbc_lblNewLabel_14);
		
		JLabel lblNewLabel_15 = new JLabel("Jogadores");
		GridBagConstraints gbc_lblNewLabel_15 = new GridBagConstraints();
		gbc_lblNewLabel_15.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_15.gridx = 7;
		gbc_lblNewLabel_15.gridy = 2;
		addTodoPanel.add(lblNewLabel_15, gbc_lblNewLabel_15);
		
		JComboBox epocaAddCB = new JComboBox();
		GridBagConstraints gbc_epocaAddCB = new GridBagConstraints();
		gbc_epocaAddCB.gridwidth = 2;
		gbc_epocaAddCB.insets = new Insets(0, 0, 5, 5);
		gbc_epocaAddCB.fill = GridBagConstraints.HORIZONTAL;
		gbc_epocaAddCB.gridx = 4;
		gbc_epocaAddCB.gridy = 3;
		addTodoPanel.add(epocaAddCB, gbc_epocaAddCB);
		try {
			ResultSet rs2; 

			rs2 = stmt.executeQuery("SELECT idEpoca FROM Epoca");
			//model3.clear();
		
			while(rs2.next()){
				epocaAddCB.addItem(rs2.getInt("idEpoca"));
			}
		}catch(SQLException se){
			se.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}
			}
		
		DefaultListModel<String> listmodel3 = new DefaultListModel<String>();
		equipaList = new JList<String>(listmodel3);
		//JList<? extends E> equipaList = new JList();
		GridBagConstraints gbc_equipaList = new GridBagConstraints();
		gbc_equipaList.gridheight = 8;
		gbc_equipaList.insets = new Insets(0, 0, 5, 5);
		gbc_equipaList.fill = GridBagConstraints.BOTH;
		gbc_equipaList.gridx = 1;
		gbc_equipaList.gridy = 3;
		addTodoPanel.add(equipaList, gbc_equipaList);
		
		//JList<? extends E> jogadorList = new JList();
		DefaultListModel<String> listmodel = new DefaultListModel<String>();
		jogadorList = new JList<String>(listmodel);
		GridBagConstraints gbc_jogadorList = new GridBagConstraints();
		gbc_jogadorList.gridwidth = 2;
		gbc_jogadorList.gridheight = 8;
		gbc_jogadorList.insets = new Insets(0, 0, 5, 0);
		gbc_jogadorList.fill = GridBagConstraints.BOTH;
		gbc_jogadorList.gridx = 6;
		gbc_jogadorList.gridy = 3;
		addTodoPanel.add(jogadorList, gbc_jogadorList);
		
		JButton btnMostrar = new JButton("Mostrar");
		btnMostrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				loadJlistJog();
				loadEquipasJlist();
			}
		});
		GridBagConstraints gbc_btnMostrar = new GridBagConstraints();
		gbc_btnMostrar.insets = new Insets(0, 0, 5, 5);
		gbc_btnMostrar.gridx = 4;
		gbc_btnMostrar.gridy = 5;
		addTodoPanel.add(btnMostrar, gbc_btnMostrar);
		
		JButton btnAdicionarTudo = new JButton("Adicionar");
		//butao atribuir jogadores as equipas nas epocas
		btnAdicionarTudo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnAdicionarTudo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//<>
				//System.out.println(part1);
				String partEquipa = equipaList.getSelectedValue();
				Integer partEpoca = (Integer) epocaAddCB.getSelectedItem();
				String sql ="INSERT INTO Pantel(idJogador, idEquipa, idEpoca) VALUES(?,?,?)";
				
				String sqlequi ="SELECT idEquipa FROM Equipa WHERE nomeEquipa = '"+partEquipa+"'";
				int id = 0;
				int id1 = 0;
				//int num=jogadorList.getSelectedIndices().length;
				//int num1=0;
				
				jogadorid=jogadorList.getSelectedValue();
				System.out.println("ver o valor do jogador id antes do split: "+jogadorid);
				String[] parts = jogadorid.split(" ");
				String part1 = parts[0]; // tera o nome do jogador
				System.out.println("print da part1"+part1);
				String part2 = parts[1];
				String sqljog="SELECT idJogador FROM Jogador WHERE nomeJogador= '"+part1+"'";//AND posicaoHabitual= '"+part2+"'";
				
				try(PreparedStatement stmt = conn.prepareStatement(sqljog)){//while(num1<num){
					ResultSet rs1=stmt.executeQuery();
					//while(rs1.next()){
						id1 = rs1.getInt(1);
						System.out.println("print do id1: "+id1);
					//}
					}catch(SQLException e3){
						e3.printStackTrace();
					}
						
						try( PreparedStatement stmt = conn.prepareStatement(sqlequi)) {
							ResultSet rs3=stmt.executeQuery();
						//	while(rs3.next()){
							id=rs3.getInt(1);
							System.out.println("valor do id da equipa: "+id);
							//}
						} catch (SQLException e2) {
							e2.printStackTrace();
						}
						
						try( PreparedStatement stmt = conn.prepareStatement(sql)) {
				            stmt.setInt(1, id1);
				            stmt.setInt(2, id);
				            stmt.setInt(3, partEpoca);
				            stmt.executeUpdate();
				            System.out.println("insert sucessfull");
				        } catch (SQLException e1) {
				            System.out.println(e1.getMessage());
				        }
					//}
					//num1++;
			}
		});
		GridBagConstraints gbc_btnAdicionarTudo = new GridBagConstraints();
		gbc_btnAdicionarTudo.insets = new Insets(0, 0, 0, 5);
		gbc_btnAdicionarTudo.gridx = 1;
		gbc_btnAdicionarTudo.gridy = 11;
		addTodoPanel.add(btnAdicionarTudo, gbc_btnAdicionarTudo);
		
		JButton btnMenu7 = new JButton("voltar ao menu");
		btnMenu7.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				addPanel.setVisible(false);
				mainPanel.setVisible(true);
			}
		});
		GridBagConstraints gbc_btnMenu7 = new GridBagConstraints();
		gbc_btnMenu7.gridx = 7;
		gbc_btnMenu7.gridy = 11;
		addTodoPanel.add(btnMenu7, gbc_btnMenu7);

		JPanel classifPanel = new JPanel();
		contentPane.add(classifPanel, "name_2420799046260962");
		GridBagLayout gbl_classifPanel = new GridBagLayout();
		gbl_classifPanel.columnWidths = new int[]{159, 0};
		gbl_classifPanel.rowHeights = new int[]{242, 23, 0};
		gbl_classifPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_classifPanel.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		classifPanel.setLayout(gbl_classifPanel);
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 0;
		classifPanel.add(scrollPane, gbc_scrollPane);
		
		Vector<String> data = new Vector();
		Vector<String> colNames = new Vector();
		colNames.add("designašao");
		colNames.add("Data de inicio");
		colNames.add("Data de FIM");
		
		classifTable = new JTable(data, colNames);
		
		scrollPane.setViewportView(classifTable);
		//scrollPane.setRowHeaderView(classifTable);

		JButton btnMenu1 = new JButton("voltar ao menu");
		btnMenu1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				classifPanel.setVisible(false);
				mainPanel.setVisible(true);
			}
		});
		GridBagConstraints gbc_btnMenu1 = new GridBagConstraints();
		gbc_btnMenu1.anchor = GridBagConstraints.NORTHWEST;
		gbc_btnMenu1.gridx = 0;
		gbc_btnMenu1.gridy = 1;
		classifPanel.add(btnMenu1, gbc_btnMenu1);



		JPanel equipaPanel = new JPanel();
		contentPane.add(equipaPanel, "name_2421597903138038");
		GridBagLayout gbl_equipaPanel = new GridBagLayout();
		gbl_equipaPanel.columnWidths = new int[]{0, 0, 0, 0};
		gbl_equipaPanel.rowHeights = new int[]{0, 0, 0, 0};
		gbl_equipaPanel.columnWeights = new double[]{0.0, 1.0, 1.0, Double.MIN_VALUE};
		gbl_equipaPanel.rowWeights = new double[]{1.0, 0.0, 0.0, Double.MIN_VALUE};
		equipaPanel.setLayout(gbl_equipaPanel);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
		gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_1.gridwidth = 3;
		gbc_scrollPane_1.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane_1.gridx = 0;
		gbc_scrollPane_1.gridy = 0;
		equipaPanel.add(scrollPane_1, gbc_scrollPane_1);

		//equipaTable = new JTable();
		
		
		Vector<String> data1 = new Vector();
		Vector<String> colNames1 = new Vector();
		colNames1.add("Nome Equipa");
		colNames1.add("Nome do Estadio");
		colNames1.add("Data Fundašao");

		equipaTable = new JTable(data1, colNames1);
		scrollPane_1.setViewportView(equipaTable);
		
		
		JPanel jogPanel = new JPanel();
		contentPane.add(jogPanel, "name_480622185526981");
		GridBagLayout gbl_jogPanel = new GridBagLayout();
		gbl_jogPanel.columnWidths = new int[]{0, 0, 155, 0, 0};
		gbl_jogPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
		gbl_jogPanel.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_jogPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		jogPanel.setLayout(gbl_jogPanel);

		JPanel melhorPanel = new JPanel();
		contentPane.add(melhorPanel, "name_481404334119056");
		GridBagLayout gbl_melhorPanel = new GridBagLayout();
		gbl_melhorPanel.columnWidths = new int[]{0, 0, 0, 0, 0};
		gbl_melhorPanel.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_melhorPanel.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_melhorPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		melhorPanel.setLayout(gbl_melhorPanel);


		JButton btnClassifica = new JButton("Tabela classificatica");
		btnClassifica.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) { 
				mainPanel.setVisible(false);
				classifPanel.setVisible(true);
			}
		});
		GridBagConstraints gbc_btnClassifica = new GridBagConstraints();
		gbc_btnClassifica.insets = new Insets(0, 0, 5, 5);
		gbc_btnClassifica.gridx = 1;
		gbc_btnClassifica.gridy = 0;
		mainPanel.add(btnClassifica, gbc_btnClassifica);

		JButton btnEquipas = new JButton("Tabelas equipas");
		btnEquipas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				mainPanel.setVisible(false);
				equipaPanel.setVisible(true);
			}
		});
		GridBagConstraints gbc_btnEquipas = new GridBagConstraints();
		gbc_btnEquipas.insets = new Insets(0, 0, 5, 0);
		gbc_btnEquipas.gridx = 2;
		gbc_btnEquipas.gridy = 0;
		mainPanel.add(btnEquipas, gbc_btnEquipas);

		JButton btnJogadores = new JButton("Tabela Jogadores");
		btnJogadores.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				mainPanel.setVisible(false);
				jogPanel.setVisible(true);
			}
		});
		GridBagConstraints gbc_btnJogadores = new GridBagConstraints();
		gbc_btnJogadores.insets = new Insets(0, 0, 5, 5);
		gbc_btnJogadores.gridx = 1;
		gbc_btnJogadores.gridy = 1;
		mainPanel.add(btnJogadores, gbc_btnJogadores);

		JButton btnMelhores = new JButton("Melhores");
		btnMelhores.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				mainPanel.setVisible(false);
				melhorPanel.setVisible(true);
			}
		});
		GridBagConstraints gbc_btnMelhores = new GridBagConstraints();
		gbc_btnMelhores.insets = new Insets(0, 0, 5, 0);
		gbc_btnMelhores.gridx = 2;
		gbc_btnMelhores.gridy = 1;
		mainPanel.add(btnMelhores, gbc_btnMelhores);

		JLayeredPane addPane = new JLayeredPane();
		GridBagConstraints gbc_addPane = new GridBagConstraints();
		gbc_addPane.gridwidth = 5;
		gbc_addPane.fill = GridBagConstraints.BOTH;
		gbc_addPane.gridx = 0;
		gbc_addPane.gridy = 3;
		mainPanel.add(addPane, gbc_addPane);
		GridBagLayout gbl_addPane = new GridBagLayout();
		gbl_addPane.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_addPane.rowHeights = new int[]{0, 0, 0, 0};
		gbl_addPane.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_addPane.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		addPane.setLayout(gbl_addPane);

		//addPane.setVisible(false);

		JButton btnAdd = new JButton("adicionar coisas");
		btnAdd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				mainPanel.setVisible(false);
				addPanel.setVisible(true);
				
			}
		});
		GridBagConstraints gbc_btnAdd = new GridBagConstraints();
		gbc_btnAdd.insets = new Insets(0, 0, 0, 5);
		gbc_btnAdd.gridx = 3;
		gbc_btnAdd.gridy = 2;
		addPane.add(btnAdd, gbc_btnAdd);
		
		
		JButton loginbtn = new JButton("login");
		loginbtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String pw = new String(txtpass.getPassword()) ;
				if(rdbtnAnonimo.isSelected()){
					loginPanel.setVisible(false);
					mainPanel.setVisible(true);
					addPane.setVisible(false);
				}else{
				if(txtlogin.getText().equals("pc") && pw.equals("pc") ){
					System.out.println("login ok");
					loginPanel.setVisible(false);
					mainPanel.setVisible(true);
					loadTableX();
					

				}else{
					lblLoginError.setText("Login Errado");
				}

			}
		}
		});
		GridBagConstraints gbc_loginbtn = new GridBagConstraints();
		gbc_loginbtn.insets = new Insets(0, 0, 5, 0);
		gbc_loginbtn.gridx = 1;
		gbc_loginbtn.gridy = 2;
		loginPanel.add(loginbtn, gbc_loginbtn);
		
		Vector<String> data11 = new Vector();
		Vector<String> colNames11 = new Vector();
		colNames11.add("Nome do jogador");
		colNames11.add("data de nascimento");
		colNames11.add("Posicao habitual");
		colNames11.add("Equipa atual");

		jogTable = new JTable(data11, colNames11);
		
		Vector<String> data2 = new Vector();
		Vector<String> colNames2 = new Vector();
		colNames2.add("Nome do jogador");
		colNames2.add("Numero Golos");
		
		JScrollPane scrollPane_2 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_2 = new GridBagConstraints();
		gbc_scrollPane_2.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_2.gridheight = 2;
		gbc_scrollPane_2.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane_2.gridx = 0;
		gbc_scrollPane_2.gridy = 0;
		melhorPanel.add(scrollPane_2, gbc_scrollPane_2);

		melhorJogTable = new JTable(data2, colNames2);
		scrollPane_2.setViewportView(melhorJogTable);

		
		
		JLabel lblEpoca_4 = new JLabel("Epoca");
		GridBagConstraints gbc_lblEpoca_4 = new GridBagConstraints();
		gbc_lblEpoca_4.insets = new Insets(0, 0, 5, 5);
		gbc_lblEpoca_4.gridx = 1;
		gbc_lblEpoca_4.gridy = 0;
		melhorPanel.add(lblEpoca_4, gbc_lblEpoca_4);

		Vector<String> data3 = new Vector();
		Vector<String> colNames3 = new Vector();
		colNames3.add("Nome da equipa");
		colNames3.add("");
		
		
		
		JComboBox comboBox_1 = new JComboBox();
		GridBagConstraints gbc_comboBox_1 = new GridBagConstraints();
		gbc_comboBox_1.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_1.gridx = 1;
		gbc_comboBox_1.gridy = 1;
		melhorPanel.add(comboBox_1, gbc_comboBox_1);
		
		try {
			ResultSet rs2; 

			rs2 = stmt.executeQuery("SELECT idEpoca FROM Epoca");
			//model3.clear();
		
			while(rs2.next()){
				comboBox_1.addItem(rs2.getInt("idEpoca"));
			}
		}catch(SQLException se){
			se.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}
			}
		
		JButton btnMelhorjog = new JButton("Melhor jogador");
		//mostra io melhor jogador
		btnMelhorjog.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int epoMelhorJ = (int) comboBox_1.getSelectedItem(); 
				//String sqlmelhorjog = "SELECT Jogador.nomeJogador, count(Marcador.autogolo) as n FROM Jogador, Marcador, Jogo, Pantel Where Jogo.idJogo = Marcador.idJogo AND Marcador.idJogador = Jogador.idJogador AND Pantel.idJogador = Marcador.idJogador AND Jogo.idEpoca = '"+epoMelhorJ+"'";
				
					
					DefaultTableModel tm3 = (DefaultTableModel) melhorJogTable.getModel();
					tm3.setRowCount(3);
					tm3.setColumnCount(2);
					
					
					
					try {
						ResultSet rs = stmt.executeQuery("SELECT Jogador.nomeJogador, SUM(Marcador.autogolo) as n FROM Jogador, Marcador, Jogo, Pantel Where Jogo.idJogo = Marcador.idJogo AND Marcador.idJogador = Jogador.idJogador AND Pantel.idJogador = Marcador.idJogador AND Jogo.idEpoca = '"+epoMelhorJ+"'");
					int l = 0;
					while(rs.next()){
						int c = 0;
						tm3.setValueAt(rs.getString("nomeJogador"), l, c++);
						tm3.setValueAt(rs.getString("n"), l, c++);
						l++;
					}
				}catch(SQLException se){
					se.printStackTrace();
				}catch(Exception e){
					e.printStackTrace();
				}finally{
					try{
						if(stmt!=null)
							stmt.close();
					}catch(SQLException se2){
					}
				}
			
				}
				
				
		});
		GridBagConstraints gbc_btnMelhorjog = new GridBagConstraints();
		gbc_btnMelhorjog.insets = new Insets(0, 0, 5, 5);
		gbc_btnMelhorjog.gridx = 0;
		gbc_btnMelhorjog.gridy = 2;
		melhorPanel.add(btnMelhorjog, gbc_btnMelhorjog);

		JButton btnMenu = new JButton("Voltar ao menu");
		btnMenu.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				equipaPanel.setVisible(false);
				mainPanel.setVisible(true);
			}
		});
		
		JLabel lblEpoca_2 = new JLabel("Epoca:");
		GridBagConstraints gbc_lblEpoca_2 = new GridBagConstraints();
		gbc_lblEpoca_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblEpoca_2.gridx = 0;
		gbc_lblEpoca_2.gridy = 1;
		equipaPanel.add(lblEpoca_2, gbc_lblEpoca_2);
		
		JComboBox equipaEpoCB = new JComboBox();
		GridBagConstraints gbc_equipaEpoCB = new GridBagConstraints();
		gbc_equipaEpoCB.insets = new Insets(0, 0, 5, 5);
		gbc_equipaEpoCB.fill = GridBagConstraints.HORIZONTAL;
		gbc_equipaEpoCB.gridx = 1;
		gbc_equipaEpoCB.gridy = 1;
		equipaPanel.add(equipaEpoCB, gbc_equipaEpoCB);
		
		
		try {
			ResultSet rs3; 
			
			rs3 = stmt.executeQuery("SELECT idEpoca FROM Epoca");
		
			while(rs3.next()){
				equipaEpoCB.addItem(rs3.getInt("idEpoca"));
			}
		}catch(SQLException se){
			se.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}
			}
		
		JButton btnMostraEquipas = new JButton("Mostrar");
		//mostrar as euipas estaticas
		btnMostraEquipas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnMostraEquipas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				
				try{
					int mostraequiepo = (int) equipaEpoCB.getSelectedItem();
					
					
					ResultSet rs = stmt.executeQuery("SELECT count(*) as n FROM Equipa,Pantel,Epoca WHERE Epoca.idEpoca = Pantel.idEpoca AND Equipa.idEquipa = Pantel.idEquipa AND Epoca.idEpoca = '"+mostraequiepo+"'");
					rs.next();
					
					DefaultTableModel tm = (DefaultTableModel) equipaTable.getModel();
					tm.setRowCount(rs.getInt("n"));
					tm.setColumnCount(3);
					

					rs = stmt.executeQuery("Select Equipa.* FROM Equipa,Pantel,Epoca WHERE Epoca.idEpoca = Pantel.idEpoca AND Equipa.idEquipa = Pantel.idEquipa AND Epoca.idEpoca ='"+mostraequiepo+"'");
					int l = 0;
					while(rs.next()){
						int c = 0;
						tm.setValueAt(rs.getString("nomeEquipa"), l, c++);
						tm.setValueAt(rs.getString("nomeEstadio"), l, c++);
						tm.setValueAt(rs.getString("dataFundacao"), l, c++);
						l++;
					}
				}catch(SQLException se){
					se.printStackTrace();
				}catch(Exception e){
					e.printStackTrace();
				}finally{
					try{
						if(stmt!=null)
							stmt.close();
					}catch(SQLException se2){
					}
				}
				
				
				
			}
		});
		GridBagConstraints gbc_btnMostraEquipas = new GridBagConstraints();
		gbc_btnMostraEquipas.insets = new Insets(0, 0, 0, 5);
		gbc_btnMostraEquipas.gridx = 1;
		gbc_btnMostraEquipas.gridy = 2;
		equipaPanel.add(btnMostraEquipas, gbc_btnMostraEquipas);
		GridBagConstraints gbc_btnMenu = new GridBagConstraints();
		gbc_btnMenu.gridx = 2;
		gbc_btnMenu.gridy = 2;
		equipaPanel.add(btnMenu, gbc_btnMenu);

		JButton btnMenu2 = new JButton("voltar ao menu");
		btnMenu2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jogPanel.setVisible(false);
				mainPanel.setVisible(true);
			}
		});
		
		JLabel lblEpoca_3 = new JLabel("Epoca");
		GridBagConstraints gbc_lblEpoca_3 = new GridBagConstraints();
		gbc_lblEpoca_3.anchor = GridBagConstraints.EAST;
		gbc_lblEpoca_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblEpoca_3.gridx = 1;
		gbc_lblEpoca_3.gridy = 1;
		jogPanel.add(lblEpoca_3, gbc_lblEpoca_3);
		
		JComboBox comboBox = new JComboBox();
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox.gridx = 2;
		gbc_comboBox.gridy = 1;
		jogPanel.add(comboBox, gbc_comboBox);
		
		
		
		try {
			ResultSet rs4; 
			
			rs4 = stmt.executeQuery("SELECT idEpoca FROM Epoca");
		
			while(rs4.next()){
				comboBox.addItem(rs4.getInt("idEpoca"));
			}
		}catch(SQLException se){
			se.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}
			}
		
		
		
		GridBagConstraints gbc_btnMenu2 = new GridBagConstraints();
		gbc_btnMenu2.insets = new Insets(0, 0, 5, 0);
		gbc_btnMenu2.gridx = 3;
		gbc_btnMenu2.gridy = 2;
		jogPanel.add(btnMenu2, gbc_btnMenu2);
		
		JButton btnMostar = new JButton("Mostar");
		//mostra os jogadores estaticos
		btnMostar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				
				try{
					int mostraequiepo = (int) comboBox.getSelectedItem();
					
					
					ResultSet rs = stmt.executeQuery("Select count(*) as n FROM Jogador, Pantel, Epoca, Equipa WHERE Epoca.idEpoca= Pantel.idEpoca AND Equipa.idEquipa = Pantel.idEquipa AND Jogador.idJogador = Pantel.idJogador AND Epoca.idEpoca = '"+mostraequiepo+"'");
					rs.next();
					DefaultTableModel tm1 = (DefaultTableModel) jogTable.getModel();
					tm1.setRowCount(rs.getInt("n"));
					tm1.setColumnCount(4);
					

					rs = stmt.executeQuery("Select Jogador.*, Equipa.nomeEquipa FROM Jogador, Pantel, Epoca, Equipa WHERE Epoca.idEpoca= Pantel.idEpoca AND Equipa.idEquipa = Pantel.idEquipa AND Jogador.idJogador = Pantel.idJogador AND Epoca.idEpoca = '"+mostraequiepo+"'");
					int l = 0;
					while(rs.next()){
						int c = 0;
						tm1.setValueAt(rs.getString("nomeJogador"), l, c++);
						tm1.setValueAt(rs.getString("dataNascimento"), l, c++);
						tm1.setValueAt(rs.getString("posicaoHabitual"), l, c++);
						tm1.setValueAt(rs.getString("nomeEquipa"), l, c++);
						l++;
					}
				}catch(SQLException se){
					se.printStackTrace();
				}catch(Exception e){
					e.printStackTrace();
				}finally{
					try{
						if(stmt!=null)
							stmt.close();
					}catch(SQLException se2){
					}
				}
				
				
			}
		});
		GridBagConstraints gbc_btnMostar = new GridBagConstraints();
		gbc_btnMostar.insets = new Insets(0, 0, 5, 5);
		gbc_btnMostar.gridx = 2;
		gbc_btnMostar.gridy = 3;
		jogPanel.add(btnMostar, gbc_btnMostar);
		
	
		GridBagConstraints gbc_jogTable = new GridBagConstraints();
		gbc_jogTable.gridwidth = 4;
		gbc_jogTable.insets = new Insets(0, 0, 0, 5);
		gbc_jogTable.gridx = 0;
		gbc_jogTable.gridy = 4;
		jogPanel.add(jogTable, gbc_jogTable);

		JButton btnMenu3 = new JButton("voltar ao menu");
		btnMenu3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				melhorPanel.setVisible(false);
				mainPanel.setVisible(true);
			}
		});
		GridBagConstraints gbc_btnMenu3 = new GridBagConstraints();
		gbc_btnMenu3.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnMenu3.insets = new Insets(0, 0, 0, 5);
		gbc_btnMenu3.gridx = 1;
		gbc_btnMenu3.gridy = 3;
		melhorPanel.add(btnMenu3, gbc_btnMenu3);
		
		JPanel addJogoPanel = new JPanel();
		contentPane.add(addJogoPanel, "name_39264475778796");
		GridBagLayout gbl_addJogoPanel = new GridBagLayout();
		gbl_addJogoPanel.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 125, 0, -11, 0, 0, 0};
		gbl_addJogoPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_addJogoPanel.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_addJogoPanel.rowWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		addJogoPanel.setLayout(gbl_addJogoPanel);
		
		JLabel lblEquipaDaCasa = new JLabel("Equipa da casa");
		GridBagConstraints gbc_lblEquipaDaCasa = new GridBagConstraints();
		gbc_lblEquipaDaCasa.gridwidth = 2;
		gbc_lblEquipaDaCasa.insets = new Insets(0, 0, 5, 5);
		gbc_lblEquipaDaCasa.gridx = 2;
		gbc_lblEquipaDaCasa.gridy = 0;
		addJogoPanel.add(lblEquipaDaCasa, gbc_lblEquipaDaCasa);
		
		JLabel lblNewLabel_13 = new JLabel("Equipa de Fora");
		GridBagConstraints gbc_lblNewLabel_13 = new GridBagConstraints();
		gbc_lblNewLabel_13.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_13.gridx = 10;
		gbc_lblNewLabel_13.gridy = 0;
		addJogoPanel.add(lblNewLabel_13, gbc_lblNewLabel_13);
		
		//JList<? extends E> ecList = new JList();
		DefaultListModel<String> listmodel2 = new DefaultListModel<String>();
		JList ecList = new JList(listmodel2);
		GridBagConstraints gbc_ecList = new GridBagConstraints();
		gbc_ecList.gridheight = 2;
		gbc_ecList.insets = new Insets(0, 0, 5, 5);
		gbc_ecList.fill = GridBagConstraints.BOTH;
		gbc_ecList.gridx = 3;
		gbc_ecList.gridy = 1;
		addJogoPanel.add(ecList, gbc_ecList);
		
		DefaultListModel<String> listmodel1 = new DefaultListModel<String>();
		
		JLabel lblEpoca = new JLabel("Epoca:");
		GridBagConstraints gbc_lblEpoca = new GridBagConstraints();
		gbc_lblEpoca.insets = new Insets(0, 0, 5, 5);
		gbc_lblEpoca.gridx = 8;
		gbc_lblEpoca.gridy = 1;
		addJogoPanel.add(lblEpoca, gbc_lblEpoca);
		
		JList efList = new JList(listmodel1);
		//JList<? extends E> efList = new JList();
		GridBagConstraints gbc_efList = new GridBagConstraints();
		gbc_efList.gridheight = 2;
		gbc_efList.insets = new Insets(0, 0, 5, 5);
		gbc_efList.fill = GridBagConstraints.BOTH;
		gbc_efList.gridx = 10;
		gbc_efList.gridy = 1;
		addJogoPanel.add(efList, gbc_efList);
		
		JComboBox epoJogoCB = new JComboBox();
		GridBagConstraints gbc_epoJogoCB = new GridBagConstraints();
		gbc_epoJogoCB.insets = new Insets(0, 0, 5, 5);
		gbc_epoJogoCB.fill = GridBagConstraints.HORIZONTAL;
		gbc_epoJogoCB.gridx = 8;
		gbc_epoJogoCB.gridy = 2;
		addJogoPanel.add(epoJogoCB, gbc_epoJogoCB);
		
		try {
			ResultSet rs2; 
			//JComboBox epocaAddCB = new JComboBox();
			//DefaultComboBoxModel<Integer> epocaModelCB = (DefaultComboBoxModel<Integer>) epocaAddCB. 
			//JComboBox epocaAddCB = new JComboBox();
			rs2 = stmt.executeQuery("SELECT idEpoca FROM Epoca");
			//model3.clear();
		
			while(rs2.next()){
				epoJogoCB.addItem(rs2.getInt("idEpoca"));
			}
		}catch(SQLException se){
			se.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}
			}
		
		
		
		
		
		JLabel lblGolos = new JLabel("Golos;");
		GridBagConstraints gbc_lblGolos = new GridBagConstraints();
		gbc_lblGolos.insets = new Insets(0, 0, 5, 5);
		gbc_lblGolos.gridx = 3;
		gbc_lblGolos.gridy = 3;
		addJogoPanel.add(lblGolos, gbc_lblGolos);
		
		JButton btnCarregarTeams = new JButton("Carregar teams");
		//carrega as teams no adicionar jogo consuante a epoca
		btnCarregarTeams.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				DefaultListModel<String> model1 = (DefaultListModel<String>) efList.getModel();
				model1.clear();
				DefaultListModel<String> model2 = (DefaultListModel<String>) ecList.getModel();
				model2.clear();
				int epo = (int) epoJogoCB.getSelectedItem();
				String sqlcarregateam ="SELECT Equipa.nomeEquipa FROM Equipa INNER JOIN Pantel ON Equipa.idEquipa=Pantel.idEquipa WHERE Pantel.idEpoca ='"+epo+"'";
				
				
				try(PreparedStatement stmt = conn.prepareStatement(sqlcarregateam)) {
					ResultSet rs = stmt.executeQuery();
					while(rs.next()){
						String save= rs.getString("nomeEquipa");
						model1.addElement(save);
						model2.addElement(save);
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
		GridBagConstraints gbc_btnCarregarTeams = new GridBagConstraints();
		gbc_btnCarregarTeams.insets = new Insets(0, 0, 5, 5);
		gbc_btnCarregarTeams.gridx = 8;
		gbc_btnCarregarTeams.gridy = 3;
		addJogoPanel.add(btnCarregarTeams, gbc_btnCarregarTeams);
		
		JLabel lblGolos_1 = new JLabel("Golos");
		GridBagConstraints gbc_lblGolos_1 = new GridBagConstraints();
		gbc_lblGolos_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblGolos_1.gridx = 10;
		gbc_lblGolos_1.gridy = 3;
		addJogoPanel.add(lblGolos_1, gbc_lblGolos_1);
		
		goloCasaTF = new JTextField();
		GridBagConstraints gbc_goloCasaTF = new GridBagConstraints();
		gbc_goloCasaTF.insets = new Insets(0, 0, 5, 5);
		gbc_goloCasaTF.fill = GridBagConstraints.HORIZONTAL;
		gbc_goloCasaTF.gridx = 3;
		gbc_goloCasaTF.gridy = 4;
		addJogoPanel.add(goloCasaTF, gbc_goloCasaTF);
		goloCasaTF.setColumns(10);
		
		goloForaTF = new JTextField();
		GridBagConstraints gbc_goloForaTF = new GridBagConstraints();
		gbc_goloForaTF.fill = GridBagConstraints.HORIZONTAL;
		gbc_goloForaTF.insets = new Insets(0, 0, 5, 5);
		gbc_goloForaTF.gridx = 10;
		gbc_goloForaTF.gridy = 4;
		addJogoPanel.add(goloForaTF, gbc_goloForaTF);
		goloForaTF.setColumns(10);
		
		JLabel lblDataDoJogo = new JLabel("data do jogo:");
		GridBagConstraints gbc_lblDataDoJogo = new GridBagConstraints();
		gbc_lblDataDoJogo.anchor = GridBagConstraints.EAST;
		gbc_lblDataDoJogo.insets = new Insets(0, 0, 5, 5);
		gbc_lblDataDoJogo.gridx = 8;
		gbc_lblDataDoJogo.gridy = 5;
		addJogoPanel.add(lblDataDoJogo, gbc_lblDataDoJogo);
		
		dataJogoTF = new JTextField();
		GridBagConstraints gbc_dataJogoTF = new GridBagConstraints();
		gbc_dataJogoTF.insets = new Insets(0, 0, 5, 5);
		gbc_dataJogoTF.fill = GridBagConstraints.HORIZONTAL;
		gbc_dataJogoTF.gridx = 8;
		gbc_dataJogoTF.gridy = 6;
		addJogoPanel.add(dataJogoTF, gbc_dataJogoTF);
		dataJogoTF.setColumns(10);
		
		JButton btnAdicionarJogo = new JButton("adicionar");
		// adiciona o jogo a bd
		btnAdicionarJogo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Integer partEpoca = (Integer) epoJogoCB.getSelectedItem();
				String eqCasa = (String) ecList.getSelectedValue();
				String eqVisitante = (String) efList.getSelectedValue();
				String data = dataJogoTF.getText();
				Integer goloCasa = Integer.parseInt(goloCasaTF.getText());
				Integer goloFora = Integer.parseInt(goloForaTF.getText());
				String sqleqCasa ="SELECT idEquipa FROM Equipa WHERE nomeEquipa = '"+eqCasa+"'";
				String sqleqFora ="SELECT idEquipa FROM Equipa WHERE nomeEquipa = '"+eqVisitante+"'";
				String sqladd ="INSERT INTO Jogo(idEqCasa, idEqVisitante, data, idEpoca, nGolosCasa, nGolosVisitante) VALUES (?,?,?,?,?,?)";
				int id=0;
				int id1=0;
				try( PreparedStatement stmt = conn.prepareStatement(sqleqCasa)) {
					ResultSet rs=stmt.executeQuery();
				//	while(rs3.next()){
					id=rs.getInt(1);
					System.out.println("valor do id da equipa da casa: "+id);
					//}
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				try( PreparedStatement stmt = conn.prepareStatement(sqleqFora)) {
					ResultSet rs1=stmt.executeQuery();
				//	while(rs3.next()){
					id1=rs1.getInt(1);
					System.out.println("valor do id da equipa de fora: "+id1);
					//}
				} catch (SQLException e2) {
					e2.printStackTrace();
				}
				try( PreparedStatement stmt = conn.prepareStatement(sqladd)) {
		            stmt.setInt(1, id);
		            stmt.setInt(2, id1);
		            stmt.setString(3, data);
		            stmt.setInt(4, partEpoca);
		            stmt.setInt(5, goloCasa);
		            stmt.setInt(6, goloFora);
		            stmt.executeUpdate();
		            System.out.println("insert sucessfull");
		        } catch (SQLException e3) {
		            System.out.println(e3.getMessage());
		        }
			}
		});
		
		
		GridBagConstraints gbc_btnAdicionarJogo = new GridBagConstraints();
		gbc_btnAdicionarJogo.insets = new Insets(0, 0, 5, 5);
		gbc_btnAdicionarJogo.gridx = 3;
		gbc_btnAdicionarJogo.gridy = 8;
		addJogoPanel.add(btnAdicionarJogo, gbc_btnAdicionarJogo);

		JButton btnAddJogos = new JButton("adicionar jogos");
		btnAddJogos.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				mainPanel.setVisible(false);
				addJogoPanel.setVisible(true);
			}
		});
		GridBagConstraints gbc_btnAddJogos = new GridBagConstraints();
		gbc_btnAddJogos.insets = new Insets(0, 0, 0, 5);
		gbc_btnAddJogos.gridx = 6;
		gbc_btnAddJogos.gridy = 2;
		addPane.add(btnAddJogos, gbc_btnAddJogos);

		JButton btnMenu8 = new JButton("Voltar ao menu");
		btnMenu8.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				addJogoPanel.setVisible(false);
				mainPanel.setVisible(true);
			}
		});
		GridBagConstraints gbc_btnMenu8 = new GridBagConstraints();
		gbc_btnMenu8.insets = new Insets(0, 0, 5, 5);
		gbc_btnMenu8.gridx = 10;
		gbc_btnMenu8.gridy = 8;
		addJogoPanel.add(btnMenu8, gbc_btnMenu8);
		
		
		JPanel addMarcadorPanel = new JPanel();
		contentPane.add(addMarcadorPanel, "name_69374267941305");
		GridBagLayout gbl_addMarcadorPanel = new GridBagLayout();
		gbl_addMarcadorPanel.columnWidths = new int[]{254, 0, 58, 113, 27, 0, 0, 0, 0, 0, 0, 0};
		gbl_addMarcadorPanel.rowHeights = new int[]{0, 0, 0, 195, 0, 0, 0};
		gbl_addMarcadorPanel.columnWeights = new double[]{1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_addMarcadorPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		addMarcadorPanel.setLayout(gbl_addMarcadorPanel);
		

		
		JLabel lblEpoca_1 = new JLabel("Epoca");
		GridBagConstraints gbc_lblEpoca_1 = new GridBagConstraints();
		gbc_lblEpoca_1.anchor = GridBagConstraints.EAST;
		gbc_lblEpoca_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblEpoca_1.gridx = 2;
		gbc_lblEpoca_1.gridy = 1;
		addMarcadorPanel.add(lblEpoca_1, gbc_lblEpoca_1);
		
		JComboBox marcadorEpoCB = new JComboBox();
		GridBagConstraints gbc_marcadorEpoCB = new GridBagConstraints();
		gbc_marcadorEpoCB.insets = new Insets(0, 0, 5, 5);
		gbc_marcadorEpoCB.fill = GridBagConstraints.HORIZONTAL;
		gbc_marcadorEpoCB.gridx = 3;
		gbc_marcadorEpoCB.gridy = 1;
		addMarcadorPanel.add(marcadorEpoCB, gbc_marcadorEpoCB);
		
		try {
			ResultSet rs2; 
	
			rs2 = stmt.executeQuery("SELECT idEpoca FROM Epoca");
			//model3.clear();
		
			while(rs2.next()){
				marcadorEpoCB.addItem(rs2.getInt("idEpoca"));
			}
		}catch(SQLException se){
			se.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}
			}
		
		JLabel lblJogo = new JLabel("Jogo");
		GridBagConstraints gbc_lblJogo = new GridBagConstraints();
		gbc_lblJogo.insets = new Insets(0, 0, 5, 5);
		gbc_lblJogo.gridx = 0;
		gbc_lblJogo.gridy = 2;
		addMarcadorPanel.add(lblJogo, gbc_lblJogo);
		
		
		JLabel lblJogador = new JLabel("jogador");
		GridBagConstraints gbc_lblJogador = new GridBagConstraints();
		gbc_lblJogador.insets = new Insets(0, 0, 5, 5);
		gbc_lblJogador.gridx = 9;
		gbc_lblJogador.gridy = 2;
		addMarcadorPanel.add(lblJogador, gbc_lblJogador);
		

		
		DefaultListModel<String> listmodel4 = new DefaultListModel<String>();
		JList<String> jogoList = new JList<String>(listmodel4);
		//JList jogoList = new JList();
		GridBagConstraints gbc_jogoList = new GridBagConstraints();
		gbc_jogoList.insets = new Insets(0, 0, 5, 5);
		gbc_jogoList.fill = GridBagConstraints.BOTH;
		gbc_jogoList.gridx = 0;
		gbc_jogoList.gridy = 3;
		addMarcadorPanel.add(jogoList, gbc_jogoList);
		
		JLabel minuto = new JLabel("Minuto");
		GridBagConstraints gbc_minuto = new GridBagConstraints();
		gbc_minuto.insets = new Insets(0, 0, 5, 5);
		gbc_minuto.gridx = 2;
		gbc_minuto.gridy = 3;
		addMarcadorPanel.add(minuto, gbc_minuto);
		
		minutoTF = new JTextField();
		GridBagConstraints gbc_minutoTF = new GridBagConstraints();
		gbc_minutoTF.insets = new Insets(0, 0, 5, 5);
		gbc_minutoTF.fill = GridBagConstraints.HORIZONTAL;
		gbc_minutoTF.gridx = 3;
		gbc_minutoTF.gridy = 3;
		addMarcadorPanel.add(minutoTF, gbc_minutoTF);
		minutoTF.setColumns(10);
		
		DefaultListModel<String> listmodel5 = new DefaultListModel<String>();
		JList<String> jogadorJogoList = new JList<String>(listmodel5);
		//JList jogadorJogoList = new JList();
		GridBagConstraints gbc_jogadorJogoList = new GridBagConstraints();
		gbc_jogadorJogoList.gridwidth = 5;
		gbc_jogadorJogoList.insets = new Insets(0, 0, 5, 0);
		gbc_jogadorJogoList.fill = GridBagConstraints.BOTH;
		gbc_jogadorJogoList.gridx = 6;
		gbc_jogadorJogoList.gridy = 3;
		addMarcadorPanel.add(jogadorJogoList, gbc_jogadorJogoList);
		
		JButton btnAtivarMarcador = new JButton("adicionar detalhes do jogo");
		btnAtivarMarcador.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				addJogoPanel.setVisible(false);
				addMarcadorPanel.setVisible(true);
				
			}
		});
		
		JButton btnMostraJogos = new JButton("Mostar jogos");
		//mostras os jogos na panel marcador para serem escolhidos
		btnMostraJogos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnMostraJogos.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
Integer epocacb = (Integer) marcadorEpoCB.getSelectedItem();
String teste ="SELECT e1.nomeEquipa as casa, Jogo.nGolosCasa as golosCasa, e2.nomeEquipa as visitante, Jogo.nGolosVisitante as golosVisitante FROM Equipa as e1, Equipa as e2, Jogo WHERE Jogo.idEpoca= '"+epocacb+"' AND Jogo.idEqCasa = e1.idEquipa AND Jogo.idEqVisitante = e2.idEquipa ";
				
				StringBuilder sBuilder = new StringBuilder();

			    final String SPACE = " ";
			    final String TRACE= "-";
				DefaultListModel<String> model = (DefaultListModel<String>) jogoList.getModel();
				model.clear();
				
				try {
					ResultSet rs3=stmt.executeQuery(teste);
					while(rs3.next()){
						sBuilder.setLength(0);
				        sBuilder
				             .append(rs3.getString("casa")).append(SPACE)
				             .append(rs3.getInt("golosCasa")).append(TRACE)
				             .append(rs3.getInt("golosVisitante")).append(SPACE)
				             .append(rs3.getString("visitante"));

				        model.addElement(sBuilder.toString());

						
					}
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				
			}
		});
		GridBagConstraints gbc_btnMostraJogos = new GridBagConstraints();
		gbc_btnMostraJogos.insets = new Insets(0, 0, 5, 5);
		gbc_btnMostraJogos.gridx = 3;
		gbc_btnMostraJogos.gridy = 2;
		addMarcadorPanel.add(btnMostraJogos, gbc_btnMostraJogos);
		
		GridBagConstraints gbc_btnAtivarMarcador = new GridBagConstraints();
		gbc_btnAtivarMarcador.insets = new Insets(0, 0, 5, 5);
		gbc_btnAtivarMarcador.gridx = 8;
		gbc_btnAtivarMarcador.gridy = 9;
		addJogoPanel.add(btnAtivarMarcador, gbc_btnAtivarMarcador);
		
		JButton btnMostarJogadores = new JButton("Mostar jogadores");
		//mostra os jogadores de cada equipa num epoca
		btnMostarJogadores.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				getTeams=jogoList.getSelectedValue();
				String[] data=getTeams.split(" ");
				String data1 = data[0];
				String data2 = data[1];
				String data3 = data[2];
				int epocb=(int) marcadorEpoCB.getSelectedItem();
				String idTeamC ="Select Equipa.idEquipa FROM Equipa WHERE nomeEquipa ='"+data1+"'";
				String idTeamF ="Select Equipa.idEquipa FROM Equipa WHERE nomeEquipa ='"+data3+"'";
				int idjogo = 0;
				int idEquipaC=0;
				int idEquipaF=0;

				try(PreparedStatement stmt = conn.prepareStatement(idTeamC)){//while(num1<num){
					ResultSet rs=stmt.executeQuery();
					//while(rs1.next()){
						idEquipaC = rs.getInt(1);
						System.out.println("print do id da Equipa da casa: "+idEquipaC);
					//}
					}catch(SQLException e3){
						e3.printStackTrace();
					}

						
				try(PreparedStatement stmt = conn.prepareStatement(idTeamF)){//while(num1<num){
					ResultSet rs2=stmt.executeQuery();
					//while(rs1.next()){
						idEquipaF = rs2.getInt(1);
						System.out.println("print do id da Equipa Visitante: "+idEquipaF);
					//}
					}catch(SQLException e3){
						e3.printStackTrace();
					}
				String idJogoS="Select Jogo.idJogo FROM Jogo WHERE Jogo.idEpoca='"+epocb+"' AND Jogo.idEqCasa='"+idEquipaC+"' And Jogo.idEqVisitante='"+idEquipaF+"'";

				
				try(PreparedStatement stmt = conn.prepareStatement(idJogoS)){//while(num1<num){
					ResultSet rs3=stmt.executeQuery();
					//while(rs1.next()){
						idjogo = rs3.getInt(1);
						System.out.println("print do id1: "+idjogo);
					//}
					}catch(SQLException e3){
						e3.printStackTrace();
					}
				
				String mostraJogadorC ="SELECT Jogador.nomeJogador FROM Jogador, Equipa, Pantel, Jogo, Epoca WHERE Jogo.idEpoca=Pantel.idEpoca AND Jogo.idEqCasa = Pantel.idEquipa AND Pantel.idJogador = Jogador.idJogador AND Equipa.idEquipa = '"+idEquipaC+"' AND Epoca.idEpoca = '"+epocb+"' AND Jogo.idJogo = '"+idjogo+"'";
				String mostraJogadorF ="SELECT Jogador.nomeJogador FROM Jogador, Equipa, Pantel, Jogo, Epoca WHERE Jogo.idEpoca=Pantel.idEpoca AND Jogo.idEqVisitante = Pantel.idEquipa AND Pantel.idJogador = Jogador.idJogador AND Equipa.idEquipa = '"+idEquipaF+"' AND Epoca.idEpoca = '"+epocb+"' AND Jogo.idJogo = '"+idjogo+"'";
				String mostraEquipaC ="SELECT Equipa.nomeEquipa FROM Jogador, Equipa, Pantel, Jogo, Epoca WHERE Jogo.idEpoca=Pantel.idEpoca AND Jogo.idEqCasa = Pantel.idEquipa AND Pantel.idJogador = Jogador.idJogador AND Equipa.idEquipa = '"+idEquipaC+"' AND Epoca.idEpoca = '"+epocb+"' AND Jogo.idJogo = '"+idjogo+"'";
				String mostraEquipaF ="SELECT Equipa.nomeEquipa FROM Jogador, Equipa, Pantel, Jogo, Epoca WHERE Jogo.idEpoca=Pantel.idEpoca AND Jogo.idEqCasa = Pantel.idEquipa AND Pantel.idJogador = Jogador.idJogador AND Equipa.idEquipa = '"+idEquipaF+"' AND Epoca.idEpoca = '"+epocb+"' AND Jogo.idJogo = '"+idjogo+"'";

				StringBuilder sBuilder = new StringBuilder();
				DefaultListModel<String> model6 = (DefaultListModel<String>) jogadorJogoList.getModel();
				model6.clear();
				final String SPACE = " ";
				String nomeJCasa;
				String nomeECasa;
				String nomeJFora;
				String nomeEFora;
				
				try{//while(num1<num){
					PreparedStatement stmt = conn.prepareStatement(mostraJogadorC);
					PreparedStatement stmt1 = conn.prepareStatement(mostraEquipaC);

					ResultSet rs4=stmt.executeQuery();
					ResultSet rs5=stmt1.executeQuery();
					

					//while(rs1.next()){
						nomeJCasa = rs4.getString(1);
						System.out.println("print do nome do jogador da casa antes do build: "+nomeJCasa);

						nomeECasa = rs5.getString(1);
						System.out.println("print do nome da equipa da casa antes do build: "+nomeECasa);
						while(rs4.next()){
							   // model.add(l, rs.getString("nomeJogador"));
							   // model.add(k, rs.getString("posicaoHabitual"));
							    sBuilder.setLength(0);
						        sBuilder
						             .append(nomeJCasa).append(SPACE)
						             .append(nomeECasa).append(SPACE);

						        model6.addElement(sBuilder.toString());
						        }
					//}
				}catch(SQLException se){
					se.printStackTrace();
				}catch(Exception e){
					e.printStackTrace();
				}finally{
					try{
						if(stmt!=null)
							stmt.close();
					}catch(SQLException se2){
					}
				}
			
				try{//while(num1<num){
					PreparedStatement stmt2 = conn.prepareStatement(mostraJogadorF);
					PreparedStatement stmt3 = conn.prepareStatement(mostraEquipaF);

					ResultSet rs6=stmt2.executeQuery();
					ResultSet rs7=stmt3.executeQuery();
					

					//while(rs1.next()){
						nomeJFora = rs6.getString(1);
						System.out.println("print do nome do jogador Visitante antes do build: "+nomeJFora);

						nomeEFora = rs7.getString(1);
						System.out.println("print do nome da equipa Visitante antes do build: "+nomeEFora);
						while(rs6.next()){
							   // model.add(l, rs.getString("nomeJogador"));
							   // model.add(k, rs.getString("posicaoHabitual"));
							    sBuilder.setLength(0);
						        sBuilder
						             .append(nomeJFora).append(SPACE)
						             .append(nomeEFora).append(SPACE);

						        model6.addElement(sBuilder.toString());
						        }
					//}
				}catch(SQLException se){
					se.printStackTrace();
				}catch(Exception e){
					e.printStackTrace();
				}finally{
					try{
						if(stmt!=null)
							stmt.close();
					}catch(SQLException se2){
					}
				}

				
				
				
			}
		});
		GridBagConstraints gbc_btnMostarJogadores = new GridBagConstraints();
		gbc_btnMostarJogadores.insets = new Insets(0, 0, 5, 5);
		gbc_btnMostarJogadores.gridx = 3;
		gbc_btnMostarJogadores.gridy = 4;
		addMarcadorPanel.add(btnMostarJogadores, gbc_btnMostarJogadores);
		
		
		JButton btnAddMarcador = new JButton("adicionar");
		//adicionar o marcador 
		btnAddMarcador.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				//adicionar marcador 
			double minuto = Double.parseDouble(minutoTF.getText());
			int numGolos=1;
			getTeams=jogoList.getSelectedValue();
			String[] data=getTeams.split(" ");
			String data1 = data[0];
			String data2 = data[1];
			String data3 = data[2];
			int epocb=(int) marcadorEpoCB.getSelectedItem();
			String idTeamC ="Select Equipa.idEquipa FROM Equipa WHERE nomeEquipa ='"+data1+"'";
			String idTeamF ="Select Equipa.idEquipa FROM Equipa WHERE nomeEquipa ='"+data3+"'";
			int idjogo = 0;
			int idEquipaC=0;
			int idEquipaF=0;

			try(PreparedStatement stmt = conn.prepareStatement(idTeamC)){//obtem o nome da equipa da casa
				ResultSet rs=stmt.executeQuery();
				//while(rs1.next()){
					idEquipaC = rs.getInt(1);
					System.out.println("print do id da Equipa da casa no adicionar: "+idEquipaC);
				//}
				}catch(SQLException e3){
					e3.printStackTrace();
				}

					
			try(PreparedStatement stmt = conn.prepareStatement(idTeamF)){//obtem o nome da equipa de fora
				ResultSet rs2=stmt.executeQuery();
				//while(rs1.next()){
					idEquipaF = rs2.getInt(1);
					System.out.println("print do id da Equipa Visitante no adicionar: "+idEquipaF);
				//}
				}catch(SQLException e3){
					e3.printStackTrace();
				}
			String idJogoS="Select Jogo.idJogo FROM Jogo WHERE Jogo.idEpoca='"+epocb+"' AND Jogo.idEqCasa='"+idEquipaC+"' And Jogo.idEqVisitante='"+idEquipaF+"'";

			
			try(PreparedStatement stmt = conn.prepareStatement(idJogoS)){//obtem o id do jogo duas equipa participaram
				ResultSet rs3=stmt.executeQuery();
				//while(rs1.next()){
					idjogo = rs3.getInt(1);
					System.out.println("print do id do jogo no adicionar: "+idjogo);
				//}
				}catch(SQLException e3){
					e3.printStackTrace();
				}
			
			
			getjogador=jogadorJogoList.getSelectedValue();
			String[] jodr=getjogador.split(" ");
			String jodr1= jodr[0];
			String jodr2= jodr[1];
			String sqladdmarcador="INSERT INTO Marcador(idJogo, idJogador, minuto, autogolo) VALUES(?,?,?,?)";
			String sqlidjogadorm="Select Jogador.idJogador FROM Jogador WHERE Jogador.nomeJogador='"+jodr1+"'";
			int idjogm = 0;
			

			try(PreparedStatement stmt = conn.prepareStatement(sqlidjogadorm)){//obtem o id do jogador selecionado
				ResultSet rs4=stmt.executeQuery();
				//while(rs1.next()){
					idjogm = rs4.getInt(1);
					System.out.println("print do id do jogador no adicionar : "+idjogm);
				//}
				}catch(SQLException e3){
					e3.printStackTrace();
				}
			
			
			try( PreparedStatement stmt = conn.prepareStatement(sqladdmarcador)) {
	            stmt.setInt(1, idjogo);
	            stmt.setInt(2, idjogm);
	            stmt.setDouble(3, minuto);
	            stmt.setInt(4, numGolos);
	            stmt.executeUpdate();
	            System.out.println("insert sucessfull");
	        } catch (SQLException e3) {
	            System.out.println(e3.getMessage());
	        }

			
			
			}
		});
		
		GridBagConstraints gbc_btnAddMarcador = new GridBagConstraints();
		gbc_btnAddMarcador.insets = new Insets(0, 0, 0, 5);
		gbc_btnAddMarcador.gridx = 0;
		gbc_btnAddMarcador.gridy = 5;
		addMarcadorPanel.add(btnAddMarcador, gbc_btnAddMarcador);
		
		JButton btnMenu9 = new JButton("voltar ao menu");
		btnMenu9.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			addMarcadorPanel.setVisible(false);
			mainPanel.setVisible(true);
			}
		});
		GridBagConstraints gbc_btnMenu9 = new GridBagConstraints();
		gbc_btnMenu9.insets = new Insets(0, 0, 0, 5);
		gbc_btnMenu9.gridx = 6;
		gbc_btnMenu9.gridy = 5;
		addMarcadorPanel.add(btnMenu9, gbc_btnMenu9);


		/******************************************************/
}

	private void loadTableX(){
		// da load da informašlao da epoca par a tabela classif so para ter alguma coisa
		try{
			ResultSet rs = stmt.executeQuery("SELECT count(*) as n FROM Epoca");
			rs.next();
			
			DefaultTableModel tm = (DefaultTableModel) classifTable.getModel();
			tm.setRowCount(rs.getInt("n"));
			tm.setColumnCount(3);
			

			rs = stmt.executeQuery("SELECT * FROM Epoca");
			int l = 0;
			while(rs.next()){
				int c = 0;
				tm.setValueAt(rs.getString("designacao"), l, c++);
				tm.setValueAt(rs.getString("dataInicio"), l, c++);
				tm.setValueAt(rs.getString("dataFim"), l, c++);
				l++;
			}
		}catch(SQLException se){
			se.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}
		}
	}
	
	private void loadJlistJog(){
		//mostra o nome do jogador com a posicao habitual no ultimo tab do adiconar
try{
		ResultSet rs;
	    StringBuilder sBuilder = new StringBuilder();

	    final String SPACE = " ";

		 //rs = stmt.executeQuery("SELECT count(*) as n FROM Jogador");
		//rs.next();
//modelo do jlist		
		DefaultListModel<String> model = (DefaultListModel<String>) jogadorList.getModel();
		model.clear();
       
		rs = stmt.executeQuery("SELECT * FROM Jogador");
		//int l = 0;
		//int k = 1;
		while(rs.next()){
			   // model.add(l, rs.getString("nomeJogador"));
			   // model.add(k, rs.getString("posicaoHabitual"));
			    sBuilder.setLength(0);
		        sBuilder
		             .append(rs.getString("nomeJogador")).append(SPACE)
		             .append(rs.getString("posicaoHabitual")).append(SPACE);

		        model.addElement(sBuilder.toString());
		       
		}
		
	}catch(SQLException se){
		se.printStackTrace();
	}catch(Exception e){
		e.printStackTrace();
	}finally{
		try{
			if(stmt!=null)
				stmt.close();
		}catch(SQLException se2){
		}
	}

	}
	private void loadEquipasJlist(){
		//mostra o nome da equipa no ultimo tab do adiconar
		try {
			ResultSet rs1; 
			DefaultListModel<String> model3 = (DefaultListModel<String>) equipaList.getModel();
			
			
			rs1 = stmt.executeQuery("SELECT * FROM Equipa");
			model3.clear();
		
			while(rs1.next()){
				model3.addElement(rs1.getString("nomeEquipa"));
			}
		}catch(SQLException se){
			se.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}
			}
		
	}


}

